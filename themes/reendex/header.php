<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reendex
 */

	/**
	 * Reendex_doctype hook
	 *
	 * @hooked reendex_doctype -  10
	 */
	do_action( 'reendex_doctype' );
?>
<head>
<?php
	/**
	 * Reendex_before_wp_head hook
	 *
	 * @hooked reendex_head -  10
	 */
	do_action( 'reendex_before_wp_head' );


?>
</head>

<body <?php body_class(); ?>>
<?php

wp_head();
		
	/**
	 * Reendex_page_start_action hook
	 *
	 * @hooked reendex_page_start -  10
	 */
	do_action( 'reendex_page_start_action' );

	/**
	 * Reendex_loader_action hook
	 *
	 * @hooked reendex_loader -  10
	 */
	do_action( 'reendex_before_header' );

	/**
	 * Reendex_header_action hook
	 *
	 * @hooked reendex_header_start -  10
	 * @hooked reendex_top_menu -  20
	 * @hooked reendex_site_branding -  30
	 * @hooked reendex_site_navigation -  40
	 * @hooked reendex_menu -  50
	 * @hooked reendex_header_end -  60
	 */
	do_action( 'reendex_header_action' );

	/**
	 * Reendex_content_start_action hook
	 *
	 * @hooked reendex_content_start -  10
	 */
	do_action( 'reendex_content_start_action' );

	get_template_part( 'content', 'breadcrumbs' );

?>
