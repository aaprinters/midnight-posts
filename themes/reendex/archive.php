<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

get_header();

	$reendex_sidebar = get_theme_mod( 'reendex_archive_page_layout', 'rightsidebar' );
if ( 'rightsidebar' === $reendex_sidebar ) {
		$reendex_sidebar = 'right';
} elseif ( 'leftsidebar' === $reendex_sidebar ) {
		$reendex_sidebar = 'left';
} else {
		$reendex_sidebar = 'no';
}
echo 'archive';
$options = reendex_get_theme_options();
$banner_image_link = reendex_get_archive_default_banner();
$archive_content_style = get_theme_mod( 'reendex_archive_content_style' );
?>

	<?php
	if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
		$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
		if ( 'disable' !== $show_comingsoon ) {
					get_template_part( 'coming', 'soon' );
					exit();
		}
	}
	?>
<div class="home-<?php echo esc_attr( $reendex_sidebar ); ?>-side">
	<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_archive_banner_show', 'disable' ) ) ) : ?>
	<div class="archive-page-header">
		<div class="container-fluid">
			<div>
				<div class="archive-nav-inline">
					<div id="parallax-section3">
						<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $banner_image_link ); ?>'); ">
						<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_archive_banner_title', 'enable' ) ) ) : ?>
					<div class="container archive-menu">
						<?php
							// the path for the daily archives.
						if ( is_day() ) {
							$reendex_id = get_the_ID();
							$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
								reendex_custom_breadcrumbs();
							$title  = esc_html__( 'Daily Archives' , 'reendex' );
						} elseif ( is_month() ) { // the path for the monthly archives.
							$reendex_id = get_the_ID();
							$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
								reendex_custom_breadcrumbs();
							$title  = esc_html__( 'Monthly Archives' , 'reendex' );
						} elseif ( is_year() ) { // the path for the yearly archives.
							$reendex_id = get_the_ID();
							$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
								reendex_custom_breadcrumbs();
							$title  = esc_html__( 'Yearly Archives' , 'reendex' );
						} else { // the path for the others cases.
							$reendex_id = get_the_ID();
							$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
								reendex_custom_breadcrumbs();
							$title  = esc_html__( 'Archives' , 'reendex' );
						}
						?>
						<h1 class="page-title"><span><?php echo esc_html( $title ); ?></span></h1>
					</div><!-- .container archive-menu -->
						<?php endif; ?>
						</div><!-- /.image1 img-overlay3 -->
					</div><!-- /#parallax-section3 -->
				</div><!-- /.archive-nav-inline -->
			</div>
		</div><!-- /.container-fluid -->
	</div><!-- /.archive-page-header -->
	<?php endif; ?>
	<div class="module"> 
		<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_archive_breaking_show', 'enable' ) ) ) :
			get_template_part( 'template-parts/content','breakingnews' );
		endif; ?>				
			<div class="row">
				<div class="container">
					<div id="primary" class="home-main-content col-xs-12 col-sm-12 col-md-9 content-area">
						<main id="main" class="site-main">
							<header class="page-header">
								<?php
									the_archive_title( '<h2 class="page-title"><span>', '</span></h2>' );
									the_archive_description( '<div class="archive-description">', '</div>' );
								?>
							</header><!-- /.page-header -->
							<div class="
							<?php
							if ( 2 == $archive_content_style ) {
								echo 'content-archive-wrapper-2';
							} elseif ( 3 == $archive_content_style ) {
								echo 'content-archive-wrapper-3';
							} elseif ( 4 == $archive_content_style ) {
								 echo 'content-archive-wrapper-4';
							} else {
								echo 'content-archive-wrapper-1'; } ?>">  
								<?php
								if ( have_posts() ) :
									/* Start the Loop */
									while ( have_posts() ) : the_post();
										get_template_part( 'template-parts/content', 'archive' );
									endwhile;
								else :
									get_template_part( 'template-parts/content', 'none' );
								endif;
								?>
							</div>
							<div class="navigation">
								<?php
								// Previous/next page navigation.
								the_posts_pagination( array(
									'prev_text'          => esc_html__( 'Previous page', 'reendex' ),
									'next_text'          => esc_html__( 'Next page', 'reendex' ),
									'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'reendex' ) . ' </span>',
								) );
								?>
								<div class="clearfix"></div>
							</div><!-- /.navigation -->                 
						</main><!-- /#main -->
					</div><!-- /#primary -->
				<?php get_sidebar(); ?><!-- SIDEBAR -->
			</div><!-- /.container --> 
		</div><!-- /.row -->                      
	</div><!-- /.module --> 
</div><!-- /.home --> 
<?php
get_footer(); ?>
