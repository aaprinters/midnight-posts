<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Reendex
 */

get_header();

	$reendex_sidebar = get_theme_mod( 'reendex_search_page_layout', 'rightsidebar' );
if ( 'rightsidebar' == $reendex_sidebar ) {
		$reendex_sidebar = 'right';
} elseif ( 'leftsidebar' == $reendex_sidebar ) {
		$reendex_sidebar = 'left';
} else {
		$reendex_sidebar = 'no';
}

$banner_image_link = reendex_get_search_default_banner();
$options = reendex_get_theme_options();
$search_content_style = get_theme_mod( 'reendex_search_content_style' );
?>
	<?php
	if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
		$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
		if ( 'disable' !== $show_comingsoon ) {
					get_template_part( 'coming', 'soon' );
					exit();
		}
	}
	?>
<div class="home-<?php echo esc_attr( $reendex_sidebar ); ?>-side">	
	<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_search_banner_show', 'disable' ) ) ) : ?>
		<div class="archive-page-header">
			<div class="container-fluid">
				<div>
					<div class="archive-nav-inline">
						<div id="parallax-section3">
							<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $banner_image_link ); ?>'); ">
								<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_search_banner_title', 'enable' ) ) ) : ?>
									<div class="container archive-menu">
										<?php
											$reendex_id = get_the_ID();
											$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
											reendex_custom_breadcrumbs();
										?>
										<h1 class="page-title">
											<span>
												<?php
												global $wp_query;
												$total_results = $wp_query->found_posts;
												if ( $total_results ) {
													// translators: %1$s = number of results. %2$s = search query.
													printf( esc_html( _n( '%1$s search result for %2$s', '%1$s search results for %2$s', absint( $total_results ), 'reendex' ) ), esc_html( $total_results ), '&ldquo;' . get_search_query() . '&rdquo;' );
												} else {
													// translators: %s = search query.
													printf( esc_html__( 'No search results for "%s"', 'reendex' ), get_search_query() );
												}
												?>
											</span>
										</h1><!-- /.page-title -->
									</div><!-- /.container archive-menu -->
								<?php endif; ?>
							</div><!-- /.image1 img-overlay3 -->
						</div><!-- /#parallax-section3 -->
					</div><!-- /.archive-nav-inline-->
				</div>
			</div><!-- /.container-fluid -->
		</div><!-- /.archive-page-header -->
	<?php endif; ?>
	<div class="module">
		<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_search_breaking_show', 'enable' ) ) ) :
			get_template_part( 'template-parts/content','breakingnews' );
		endif; ?>
		<div class="row">
			<div class="container">
					<div id="primary" class="home-main-content col-xs-12 col-sm-12 col-md-8 content-area">
						<main id="main" class="site-main">
							<div class="
							<?php
							if ( 2 == $search_content_style ) {
								echo 'content-archive-wrapper-2';
							} elseif ( 3 == $search_content_style ) {
								echo 'content-archive-wrapper-3';
							} elseif ( 4 == $search_content_style ) {
								 echo 'content-archive-wrapper-4';
							} else {
								echo 'content-archive-wrapper-1'; } ?>">  
								<?php
								if ( have_posts() ) :
									/* Start the Loop */
									while ( have_posts() ) : the_post();
										get_template_part( 'template-parts/content', 'search' );
									endwhile;
								else :
									get_template_part( 'template-parts/content', 'none' );
								endif;
								?>
							</div>                                
							<div class="navigation">
								<?php
								// Previous/next page navigation.
								the_posts_pagination( array(
									'prev_text'          => esc_html__( 'Previous page', 'reendex' ),
									'next_text'          => esc_html__( 'Next page', 'reendex' ),
									'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'reendex' ) . ' </span>',
								) );
								?>
								<div class="clearfix"></div>
							</div><!-- /.navigation -->
						</main><!-- /#main -->
					</div><!-- /#primary -->
					<?php //get_sidebar(); ?><!-- SIDEBAR -->

					<div class="left-sidebar">
						<?php if ( is_active_sidebar( 'innerpage_articles' ) ) : ?>
							<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
								<?php dynamic_sidebar( 'innerpage_articles' ); ?>
							</div><!-- #primary-sidebar -->
						<?php endif; ?>
						<?php if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
							<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
								<?php dynamic_sidebar( 'innerpage_1' ); ?>
							</div><!-- #common-sidebar -->
						<?php endif; ?>
					</div> 



			</div><!-- /.container -->
		</div><!-- /.row -->
	</div><!-- /.module -->
</div><!-- /.home -->	
<?php
get_footer();
