<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

get_header();
$options = reendex_get_theme_options();
$banner_blog_image_link = reendex_get_default_blog_banner();
$banner_title = get_option( 'reendex_blog_banner_title' );
$banner_title = empty( $banner_title )? esc_html__( 'News from reality', 'reendex' ): $banner_title;
$banner_subtitle = get_option( 'reendex_blog_banner_subtitle' );
$banner_subtitle = empty( $banner_subtitle )? esc_html__( 'Look for more news from our own sources', 'reendex' ): $banner_subtitle;
$banner_subtitle_link = $options['reendex_banner_subtitle_link'];
$blog_content_style = get_theme_mod( 'reendex_blog_content_style' );
	$reendex_sidebar = get_theme_mod( 'reendex_blog_page_layout','rightsidebar' );
if ( 'rightsidebar' == $reendex_sidebar ) {
		$reendex_sidebar = 'right';
} elseif ( 'leftsidebar' == $reendex_sidebar ) {
		$reendex_sidebar = 'left';
} else {
		$reendex_sidebar = 'no';
}
?>
	<?php
	if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
		$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
		if ( 'disable' !== $show_comingsoon ) {
					get_template_part( 'coming', 'soon' );
					exit();
		}
	}
	?>
<div class="home-<?php echo esc_attr( $reendex_sidebar ); ?>-side">
	<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_blog_banner_show', 'disable' ) ) ) : ?>
		<div class="blog-page-header">
			<div class="archive-page-header">
				<div class="container-fluid">
					<div>
						<div class="archive-nav-inline">
							<div id="parallax-section3">
								<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $banner_blog_image_link ); ?>'); ">
									<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_blog_banner_title_show', 'enable' ) ) ) : ?>
										<div class="container archive-menu">
											<h1 class="page-title"><span><?php echo esc_html( $banner_title ); ?></span></h1>
											<h4 class="page-subtitle"><span><a href="<?php echo esc_url( $banner_subtitle_link ); ?>"><?php echo esc_html( $banner_subtitle ); ?></a></span></h4>
										</div>
									<?php endif; ?>								
								</div><!-- /.image1 img-overlay3 -->
							</div><!-- /#parallax-section3 -->
						</div><!-- /.archive-nav-inline -->
					</div>
				</div><!-- /.container-fluid -->
			</div><!-- /.archive-page-header-->
		</div><!-- /.blog-page-header -->
	<?php endif; ?>
	<div class="module">
		<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_blog_breaking_show', 'enable' ) ) ) :
			get_template_part( 'template-parts/content','breakingnews' );
		endif; ?>
		<div class="row">
			<div class="container">
				<div id="primary" class="home-main-content col-xs-12 col-sm-12 col-md-9 content-area">
					<main id="main" class="site-main">
						<?php
						if ( have_posts() ) :
							if ( is_home() && ! is_front_page() ) : ?>
								<header>
									<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
								</header>
							<?php endif; ?>
							<div class="
							<?php
							if ( 2 == $blog_content_style ) {
								echo 'content-archive-wrapper-2';
							} elseif ( 3 == $blog_content_style ) {
								echo 'content-archive-wrapper-3';
							} elseif ( 4 == $blog_content_style ) {
								 echo 'content-archive-wrapper-4';
							} else {
								echo 'content-archive-wrapper-1'; } ?>"> 							
							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_format() );
							endwhile;
							else :
								get_template_part( 'template-parts/content', 'none' );
						endif; ?>
						</div>
						<div class="pagination">
							<?php
								the_posts_pagination();
							?>
						</div><!-- /.pagination -->
					</main><!-- /#main -->
				</div><!-- /#primary -->
				<?php get_sidebar(); ?><!-- SIDEBAR -->
			</div> <!-- /.container-->
		</div><!-- /.row -->
	</div><!-- /.module-->
</div><!-- /.home -->
<?php get_footer(); ?>
