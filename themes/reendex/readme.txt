﻿= Reendex Theme =

* by Via-Theme 

https://themeforest.net/user/via-theme

Thank you for purchasing our theme. If you have any questions, feature requests or anything else, always feel free to get in touch with us via our user page. We'll get back to you in less than 24 hours, and we’ll do our best to help. Thank you very much!