<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

get_header();
$banner_image_link = reendex_get_default_banner();
$options = reendex_get_theme_options();
?>
	<?php
	if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
		$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
		if ( 'disable' !== $show_comingsoon ) {
					get_template_part( 'coming', 'soon' );
					exit();
		}
	}
	?>
		<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_banner_show', 'enable' ) ) ) : ?>
			<div class="archive-page-header">
				<div class="container-fluid">
					<div>
						<div class="archive-nav-inline">
							<div id="parallax-section3">
								<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $banner_image_link ); ?>'); ">
									<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_banner_title', 'enable' ) ) ) : ?>
										<div class="container archive-menu">
											<?php
												$reendex_id = get_the_ID();
												$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
												reendex_custom_breadcrumbs();
											?>
											<h1 class="page-title"><span><?php the_title();?></span></h1>
										</div>
									<?php endif; ?>
								</div><!-- /.image1 img-overlay3 -->
							</div><!-- /#parallax-section3 -->
						</div><!-- /.archive-nav-inline -->
					</div>
				</div><!-- /.container-fluid -->
			</div><!-- /.archive-page-header -->
		<?php endif; ?>
	<div id="primary" class="content-area">
		<div class="container single-page">
			<main id="main" class="site-main">
			<?php
				$reendex_id = get_the_ID();
				$reendex_page_breadcrumbs = get_post_meta( $reendex_id, 'reendex_page_breadcrumbs', true );
			if ( 'show' == $reendex_page_breadcrumbs ) {
					 reendex_custom_breadcrumbs();
			}
			?>
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endwhile; // End of the loop.
			?>
			</main><!-- /#main -->
		</div><!-- /.container single-page -->
	</div><!-- /#primary -->
<?php
get_footer();
