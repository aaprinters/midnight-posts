<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

?>

<section class="no-results not-found">
	

	<div class="no_results_image">
		<img src="http://allroundview.com/wp-content/uploads/2019/09/Content-Under-Process-All-Round-View-File-Photo-0001.jpg">
	</div>
	<div class="no_results_text">
	<div class="module-title">
		<h1 class="page-title"><?php esc_html_e( 'Content under Process...', 'reendex' ); ?></h1>
	</div><!-- /.module-title -->
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
			<p><?php printf( /* translators: %1$s : description of url */
				wp_kses( esc_html__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'reendex' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				),
			esc_url( admin_url( 'post-new.php' ) ) ); ?>
			</p>
		<?php elseif ( is_search() ) : ?>
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'reendex' ); ?></p>
			<?php
				get_search_form();
		else : ?>
			<p><?php esc_html_e( 'The content for this section is under process and will be available soon. We apologize for the inconvenience caused.', 'reendex' ); ?></p>
			<p><?php esc_html_e( 'Perhaps, Search may help!', 'reendex' ); ?></p>
			<?php
				get_search_form();
				?><p><br></p><?php
		endif; ?>
	</div><!-- /.page-content -->
	</div><!-- /.page-content -->
</section><!-- /.no-results -->
