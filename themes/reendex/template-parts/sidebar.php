<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reendex
 */

$reendex_sidebar = '';
if ( is_search() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_search_page_layout', 'rightsidebar' );
} elseif ( is_category() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_category_archive_layout', 'rightsidebar' );
} elseif ( is_tag() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_tag_archive_layout', 'rightsidebar' );
} elseif ( is_archive() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_archive_page_layout', 'rightsidebar' );
} elseif ( is_single() ) {
	$reendex_sidebar = get_theme_mod( 'reendex_single_post_layout', 'rightsidebar' );
} else {
	$reendex_sidebar = get_theme_mod( 'reendex_blog_page_layout','rightsidebar' );
}

if ( ! $reendex_sidebar ) {
	$reendex_sidebar = 'rightsidebar';
}
if ( 'nosidebar' == $reendex_sidebar ) {
	return;
}

/*if ( 'rightsidebar' == $reendex_sidebar && is_active_sidebar( 'sidebar-1' ) ) { ?>
		<section id="secondaryright" class="home-right-sidebar widget-area col-xs-12 col-sm-12 col-md-4" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</section><!-- #secondaryright -->
	<?php
} */
 
if ( 'leftsidebar' == $reendex_sidebar && is_active_sidebar( 'leftsidebar' ) ) { ?>

		<section id="secondaryleft" class="home-left-sidebar widget-area col-xs-12 col-sm-12 col-md-3" role="complementary">
			<?php dynamic_sidebar( 'leftsidebar' ); ?>
		</section><!-- #secondaryleft -->
	<?php
}
