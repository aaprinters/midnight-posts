<?php
/**
 * Template part for main top bar
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

$current_user = wp_get_current_user();
$options = reendex_get_theme_options();
?>
	<?php
	$show_social = get_theme_mod( 'reendex_topbar_socialmedia_show', 'enable' );
	if ( 'enable' === $show_social ) :
	?>
	<ul class="left-top-menu">
		<?php $options = reendex_get_theme_options();
		$reendex_date_format = $options['reendex_date_format'];
		$reendex_time_type = get_theme_mod( 'reendex_time_type', 'client-side' );
		if ( get_theme_mod( 'reendex_date_display', 0 ) == 1 ) :
			if ( 'cshow' == $reendex_clock || '' == $reendex_clock ) { ?>
				<div class="clock"> 
					<?php if ( 'client-side' === $reendex_time_type ) { ?>
						<div id="time"></div> 
					<?php } else {
						echo '<div id="time">' . date_i18n( get_option( 'time_format' ) ) . '</div>';
} ?>
					<div id="date"><?php echo esc_html( date_i18n( $reendex_date_format ) ); ?></div>                                 
				</div>
			<?php
			}
		endif; ?>
						
		<?php
			$facebook = $options['reendex_facebook_link'];
			$twitter = $options['reendex_twitter_link'];
			$g = $options['reendex_googleplus_link'];
			$linkedin = $options['reendex_linkedin_link'];
			$pinterest = $options['reendex_pinterest_link'];
			$tumblr = $options['reendex_tumblr_link'];
			$reddit = $options['reendex_reddit_link'];
			$stumbleupon = $options['reendex_stumbleupon_link'];
			$digg = $options['reendex_digg_link'];
			$vimeo = $options['reendex_vimeo_link'];
			$youtube = $options['reendex_youtube_link'];
			$rss = $options['reendex_rss_link'];
			$instagram = $options['reendex_instagram_link'];
		if ( strlen( $facebook ) > 0 ) {
			echo '<li class="facebook"><a class="facebook" href="' . esc_url( $facebook ) . '"><i class="fa fa-facebook"></i></a></li>';
		}
		if ( strlen( $twitter ) > 0 ) {
			echo '<li class="twitter"><a class="twitter" href="' . esc_url( $twitter ) . '"><i class="fa fa-twitter"></i></a></li>';
		}
		if ( strlen( $g ) > 0 ) {
			echo '<li class="gplus"><a class="google-plus" href="' . esc_url( $g ) . '"><i class="fa fa-google-plus"></i></a></li>';
		}
		if ( strlen( $linkedin ) > 0 ) {
			echo '<li class="linkedin"><a class="linkedin" href="' . esc_url( $linkedin ) . '"><i class="fa fa-linkedin"></i></a></li>';
		}
		if ( strlen( $pinterest ) > 0 ) {
			echo '<li class="pinterest"><a class="pinterest" href="' . esc_url( $pinterest ) . '"><i class="fa fa-pinterest-p"></i></a></li>';
		}
		if ( strlen( $tumblr ) > 0 ) {
			echo '<li class="tumblr"><a class="tumblr" href="' . esc_url( $tumblr ) . '"><i class="fa fa-tumblr"></i></a></li>';
		}
		if ( strlen( $reddit ) > 0 ) {
			echo '<li class="reddit"><a class="reddit" href="' . esc_url( $reddit ) . '"><i class="fa fa-reddit"></i></a></li>';
		}
		if ( strlen( $stumbleupon ) > 0 ) {
			echo '<li class="stumbleupon"><a class="stumbleupon" href="' . esc_url( $stumbleupon ) . '"><i class="fa fa-stumbleupon"></i></a></li>';
		}
		if ( strlen( $digg ) > 0 ) {
			echo '<li class="digg"><a class="digg" href="' . esc_url( $digg ) . '"><i class="fa fa-digg"></i></a></li>';
		}
		if ( strlen( $vimeo ) > 0 ) {
			echo '<li class="vimeo"><a class="vimeo" href="' . esc_url( $vimeo ) . '"><i class="fa fa-vimeo-square"></i></a></li>';
		}
		if ( strlen( $youtube ) > 0 ) {
			echo '<li class="youtube"><a class="youtube" href="' . esc_url( $youtube ) . '"><i class="fa fa-youtube"></i></a></li>';
		}
		if ( strlen( $rss ) > 0 ) {
			echo '<li class="rss"><a class="rss" href="' . esc_url( $rss ) . '"><i class="fa fa-rss"></i></a></li>';
		}
		if ( strlen( $instagram ) > 0 ) {
			echo '<li class="instagram"><a class="instagram" href="' . esc_url( $instagram ) . '"><i class="fa fa-instagram"></i></a></li>';
		}
		?>
	</ul><!-- /.left-top-menu -->
		<?php endif;?>
	
	<ul>
		<?php $top_phone = get_theme_mod( 'reendex_top_phone' ); if ( ! empty( $top_phone ) ) : ?>
			<li class="address">
				<span><a href="callto:<?php echo esc_url( $top_phone );?>"><i class="fa fa-phone"></i> <?php esc_html_e( 'Call Us:','reendex' ); ?> <?php echo esc_html( $top_phone );?></a></span>
			</li>
		<?php endif; ?>
		<?php $top_email = get_theme_mod( 'reendex_top_email' ); if ( ! empty( $top_email ) ) : ?>
			<li class="address">
				<span><a href="mailto:<?php echo esc_url( antispambot( $top_email ) );?>"><i class="fa fa-envelope"></i> <?php esc_html_e( 'Mail Us:','reendex' ); ?> <?php echo esc_html( $top_email );?></a></span>
			</li>
		<?php endif; ?>
	</ul>
	
	<ul class="right-top-menu pull-right"> 
		<?php
			wp_nav_menu( array( 
				'theme_location' => 'header-top-menu', 
				'container_class' => 'headertop-menu-class' ) ); 
		?>
		 <?php $location_link = get_theme_mod( 'reendex_top_address' ); if ( ! empty( $location_link ) ) : ?>
			<li class="contact">
				<a><i class="fa fa-map-marker fa-i"></i> <?php echo esc_html( $location_link ); ?></a>
			</li>
		<?php endif; ?>
	
		<?php
		if ( is_user_logged_in() ) {
			echo '<li class="about">
				<a href="' . esc_url( get_author_posts_url( $current_user->ID ) ) . '"><i class="fa fa-user fa-i"></i></a> 
				   </li> ';
		} else {
			echo '<li class="about">
				<a href="' . esc_url( wp_login_url( get_permalink() ) ) . '"><i class="fa fa-sign-in"></i></a> 
			</li> ';
		} 
		?>	
		<li> 
			<?php
				$show_search_in_header = $options['reendex_show_search_in_header'];
			if ( true === $show_search_in_header ) : ?>
				<div class="header-search-box">
					<a href="#" class="search-icon"><i class="fa fa-search"></i></a>
					<div class="search-box-wrap">
						<div class="search-close-btn"><i class="pe-7s-close"></i></div>
						<?php get_search_form(); ?>
					</div>
				</div>  <!-- /.header-search-box -->
			<?php endif; ?>                              
		</li>                
	</ul><!-- /.right-top-menu pull-right -->                         
