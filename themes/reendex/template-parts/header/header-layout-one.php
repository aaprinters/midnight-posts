<?php
/**
 * Header style 1
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_top_menu' ) ) :
	/**
	 * Top Menu codes
	 */
	function reendex_top_menu() {
		?>
		<div class="top-menu"> 
			<div class="container">        
				<?php get_template_part( 'template-parts/content', 'topbar' ); ?>
			</div>                     
		</div>                 
		<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_top_menu', 20 );

if ( ! function_exists( 'reendex_site_branding' ) ) :
	/**
	 * Site branding
	 */
	function reendex_site_branding() {
	?>
		<div class="container">
			<div class="logo-ad-wrapper clearfix">
				<?php
				get_template_part( 'template-parts/header/header', 'site-branding-logo' );
				get_template_part( 'template-parts/header/header', 'site-ad-banner' );
				?>
			</div><!-- /.logo-ad-wrapper clearfix -->
		</div><!-- /.container -->
		<div class="container-fluid">
			<?php do_action( 'reendex_menu' ); ?>
		</div>
	<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_branding', 30 );


if ( ! function_exists( 'reendex_site_navigation' ) ) :
	/**
	 * Site navigation codes
	 */
	function reendex_site_navigation() {
		get_template_part( 'template-parts/header/header', 'main-menu' );
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_navigation', 40 );
