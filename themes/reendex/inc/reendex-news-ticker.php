<?php
/**
 * The template for displaying the News Ticker with RSS Feed
 *
 * @package Reendex
 */

/**
 * Default color styles.
 */
function ticker_color_styles() {
	return array(
		'Grey'      => 'grey',
		'Dark'      => 'dark',
		'Simple'    => 'modern',
	);
}
/**
 * Default ticker settings.
 */
function ticker_defaults() {
	return array(
		'title'         => '',
		'urls'          => '',
		'count'         => 5,
		'show_date'     => 1,
		'show_desc'     => 1,
		'open_newtab'   => 1,
		'strip_desc'    => 100,
		'read_more'     => '[...]',
		'color_style'   => 'modern',
		'enable_ticker' => 1,
		'visible_items' => 5,
		'strip_title'   => 0,
		'ticker_speed'  => 4,
	);
}

/**
 * The main RSS Parser.
 *
 * @param array $instance Settings for News Ticker instance.
 */
function ticker_rss_parser( $instance ) {
	$instance = wp_parse_args( $instance, ticker_defaults() );
	$urls = stripslashes( $instance['urls'] );
	$count = intval( $instance['count'] );
	$show_date = intval( $instance['show_date'] );
	$show_desc = intval( $instance['show_desc'] );
	$open_newtab = intval( $instance['open_newtab'] );
	$strip_desc = intval( $instance['strip_desc'] );
	$strip_title = intval( $instance['strip_title'] );
	$read_more = htmlspecialchars( $instance['read_more'] );
	$color_style = stripslashes( $instance['color_style'] );
	$enable_ticker = intval( $instance['enable_ticker'] );
	$visible_items = intval( $instance['visible_items'] );
	$ticker_speed = intval( $instance['ticker_speed'] ) * 1000;

	if ( empty( $urls ) ) {
		return '';
	}

	$rand = array();
	$url = explode( ',', $urls );
	$ucount = count( $url );
	for ( $i = 0; $i < $ucount; $i++ ) {
		// Get the Feed URL.
		$feed_url = trim( $url[ $i ] );
		if ( isset( $url[ $i ] ) ) {
			$rss = fetch_feed( $feed_url );
		} else {
			return '';
		}

		if ( method_exists( $rss, 'enable_order_by_date' ) ) {
			$rss->enable_order_by_date( false );
		}

		// Check for feed errors.
		if ( ! is_wp_error( $rss ) ) {
			$maxitems  = $rss->get_item_quantity( $count );
			$rss_items = $rss->get_items( 0, $maxitems );
			$rss_title = esc_attr( strip_tags( $rss->get_title() ) );
			$rss_desc  = esc_attr( strip_tags( $rss->get_description() ) );
		} else {
			echo '<div class="ticker-wrap ticker-style-' . esc_attr( $color_style ) . '" data-id="ticker-tab-' . esc_attr( $rand[ $i ] ) . '"><p>RSS Error: ' . esc_attr( $rss )->get_error_message() . '</p></div>';
			continue;
		}

		$rand_attr = isset( $rand[ $i ] ) ? ' data-id="ticker-tab-' . $rand[ $i ] . '" ' : '';

		// Outer Wrap start.
		echo '<div class="ticker-wrap ' . ( ( 1 == $enable_ticker ) ? 'ticker-vticker' : '' ) . ' ticker-style-' . esc_attr( $color_style ) . '" data-visible="' . esc_attr( $visible_items ) . '" data-speed="' . esc_attr( $ticker_speed ) . '"' . wp_kses_post( $rand_attr ) . '><div>';

		// Check feed items.
		if ( 0 == $maxitems ) {
			echo '<div>No items.</div>';
		} else {
			$count = 1;
			// Loop through each feed item.
			foreach ( $rss_items as $item ) {
				// Get the link.
				$link = $item->get_link();
				while ( stristr( $link, 'http' ) != $link ) {
					$link = substr( $link, 1 ); }
				$link = esc_url( strip_tags( $link ) );

				// Get the item title.
				$title = esc_attr( strip_tags( $item->get_title() ) );
				if ( empty( $title ) ) {
					$title = esc_html__( 'No Title', 'reendex' );
				}
				if ( 0 != $strip_title ) {
					$title_len = strlen( $title );
					$title = wp_html_excerpt( $title, $strip_title );
					$title = ( $title_len > $strip_title ) ? $title . ' ...' : $title;
				}

				// Open links in new tab.
				$newtab = ( $open_newtab ) ? ' target="_blank"' : '';

				// Get the date.
				$date = $item->get_date( 'F j, Y' );

				// Get the description.
				$desc = '';
					$rmore = '';
					$desc  = str_replace( array( "\n", "\r" ), ' ', esc_attr( strip_tags( html_entity_decode( $item->get_description(), ENT_QUOTES, get_option( 'blog_charset' ) ) ) ) );
				if ( 0 != $strip_desc ) {
						$desc  = wp_html_excerpt( $desc, $strip_desc );
						$rmore = ( ! empty( $read_more ) ) ?  '<a class="readmore" href="' . $link . '" title="Read more"' . $newtab . '>' . $read_more . '</a>' : '';

					if ( '[...]' == substr( $desc, -5 ) ) {
						$desc = substr( $desc, 0, -5 );
					} elseif ( '[&hellip;]' != substr( $desc, -10 ) ) {
						$desc .= '';
						$desc = esc_html( $desc );
					}
				}

					$desc = $desc . ' ' . $rmore;

				echo "\n\n\t";

				// Display the feed items.
				$odd_or_even = ($count % 2) ? 'odd' : 'even'; ?>
				<div class="ticker-item <?php echo esc_attr( $odd_or_even ); ?>">
					<?php if ( $show_date && ! empty( $date ) ) : ?>
						<div class="ticker-date"><?php echo esc_attr( $date ); ?></div>
					<?php endif; // End display date. ?>
					<div class="ticker-title"><a href="<?php echo esc_url( $link ); ?>" target="<?php echo esc_attr( $newtab ); ?>"><?php echo esc_html( $title ); ?></a></div>
					<?php if ( $show_desc ) : ?>
						<p class="ticker-summary ticker-clearfix"><?php echo wp_kses_post( $desc ); ?></p>
					<?php endif;  // End display summary. ?>
				</div>
				
				<?php
				// End display.
				$count++;
			} // End foreach().
		} // End if().

		// Outer wrap end.
		echo "\n\n</div>
		</div> \n\n" ;

		if ( ! is_wp_error( $rss ) ) {
			$rss->__destruct();
		}
		unset( $rss );

	} // End for().
}
