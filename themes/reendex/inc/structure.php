<?php
/**
 * Reendex basic theme structure hooks
 *
 * This file contains structural hooks.
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_doctype' ) ) :
	/**
	 * Doctype Declaration.
	 */
	function reendex_doctype() {
	?>
		<!DOCTYPE html>
			<html <?php language_attributes(); ?>>
	<?php
	}
endif;
add_action( 'reendex_doctype', 'reendex_doctype', 10 );

if ( ! function_exists( 'reendex_head' ) ) :
	/**
	 * Header Codes
	 */
	function reendex_head() {
		?>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php
	}
endif;
add_action( 'reendex_before_wp_head', 'reendex_head', 10 );

	/**
	 * Sticky menu options.
	 */
function reendex_enqueue_sticky_menu() {
	$reendex_sticky_menu = get_theme_mod( 'reendex_sticky_menu', 'enable' );
	if ( 'enable' === $reendex_sticky_menu ) {
		$header_nav_js = '';
		$header_nav_js .= "
			// Sticky navigation on scroll.
			( function ( $ ) {
				'use strict';
				$( document ).ready( function () {
					// headroom
					$( '#fixed-navbar' ).headroom( {
						tolerance: 5,
						offset: $( '#content' ).offset().top,
						classes: {
							pinned: 'headroom-pinned',
							unpinned: 'headroom-unpinned'
						}
					} );

					// affix.
					$( '#nav-wrapper' ).height( $( 'nav' ).height() );
					$( 'nav' ).affix({
						offset: { top: $( 'nav' ).offset().top }
					});
				} );
			}( jQuery ) );
		";
		wp_add_inline_script( 'reendex-main', $header_nav_js );
	}
}
add_action( 'wp_enqueue_scripts', 'reendex_enqueue_sticky_menu' );

	/**
	 * Sticky mobile menu options.
	 */
function reendex_enqueue_sticky_mobile_menu() {
	$reendex_sticky_mobile_menu = get_theme_mod( 'reendex_sticky_mobile_menu', 'disable' );
	if ( 'enable' === $reendex_sticky_mobile_menu ) {
		$mobile_menu_js = '';
		$mobile_menu_js .= "
			// Sticky mobile menu on scroll.
			( function ( $ ) {
				'use strict';
				$( document ).ready( function () {
					// headroom.
					$( '#mobile-nav-wrapper' ).headroom({
						offset: $( '#content' ).offset().top,
						classes: {
							pinned: 'headroom-pinned',
							unpinned: 'headroom-unpinned'
						}
					});

					// affix.
					$( '#mobile-nav-outer-wrapper' ).height( $( '#mobile-nav-wrapper' ).outerHeight( true ) );
					$( '#mobile-nav-wrapper' ).affix({
						offset: { top: $( '#mobile-nav-wrapper' ).offset().top }
					});
				} );
			}( jQuery ) );
		";
		wp_add_inline_script( 'reendex-main', $mobile_menu_js );
	}
}
add_action( 'wp_enqueue_scripts', 'reendex_enqueue_sticky_mobile_menu' );

if ( ! function_exists( 'reendex_page_start' ) ) :
	/**
	 * Page starts html codes
	 */
	function reendex_page_start() {
		$theme_color = get_post_meta( get_the_ID(),'reendex_primary_color',true );
		if ( '' !== $theme_color ) {
			$theme_color = $theme_color;
		} else {
			$theme_color = get_option( 'theme_color' );
		}
		?>
		<div id="page" class="site" data-color="<?php echo esc_attr( $theme_color );?>">
		<?php
	}
endif;

add_action( 'reendex_page_start_action', 'reendex_page_start', 10 );

if ( ! function_exists( 'reendex_page_end' ) ) :
	/**
	 * Page end html codes
	 */
	function reendex_page_end() {
		?>
		</div><!-- #page -->
		<?php
	}
endif;
add_action( 'reendex_page_end_action', 'reendex_page_end', 10 );

if ( ! function_exists( 'reendex_header_start' ) ) :
	/**
	 * Header start html codes
	 */
	function reendex_header_start() {
		$above_header_ad_enable = get_theme_mod( 'reendex_above_header_ad_enable' );
		if ( 'enable' == $above_header_ad_enable ) :
		?>
		<div class="reendex-above-header-ads">
			<?php do_action( 'reendex_above_header_ad' ); ?>
		</div>
		<?php endif; ?>
		<header id="header">
			<div class="wrapper">
		<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_header_start', 10 );

if ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-two' ) :
	get_template_part( 'template-parts/header/header-layout-two' );
elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-three' ) :
	get_template_part( 'template-parts/header/header-layout-three' );
elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-four' ) :
	get_template_part( 'template-parts/header/header-layout-four' );
elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-five' ) :
	get_template_part( 'template-parts/header/header-layout-five' );
elseif ( get_theme_mod( 'reendex_header_layout' ) == 'reendex-header-layout-six' ) :
	get_template_part( 'template-parts/header/header-layout-six' );
else :
	get_template_part( '/template-parts/header/header-layout-one' );
endif;

if ( ! function_exists( 'reendex_menu' ) ) :
	get_template_part( 'template-parts/header/header', 'mobile-menu' );
endif;
add_action( 'reendex_header_action', 'reendex_menu', 50 );

if ( ! function_exists( 'reendex_header_end' ) ) :
	/**
	 * Header end html codes
	 */
	function reendex_header_end() {
		?>
			</div><!--.wrapper-->
		</header><!-- #header -->
		<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_header_end', 60 );

if ( ! function_exists( 'reendex_get_default_banner' ) ) {
	/**
	 * Get Default Archive and Search Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_default_banner() {
		$banner_image_link = get_option( 'reendex_banner_image' );
		return empty( $banner_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $banner_image_link;
	}
}

if ( ! function_exists( 'reendex_get_archive_default_banner' ) ) {
	/**
	 * Get Default Archive and Search Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_archive_default_banner() {
		$banner_image_link = get_option( 'reendex_archive_banner_image' );
		return empty( $banner_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $banner_image_link;
	}
}

if ( ! function_exists( 'reendex_get_tag_default_banner' ) ) {
	/**
	 * Get Default Tag Archive Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_tag_default_banner() {
		$tag_banner_image_link = get_option( 'reendex_tag_banner_image' );
		return empty( $tag_banner_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $tag_banner_image_link;
	}
}

if ( ! function_exists( 'reendex_get_category_default_banner' ) ) {
	/**
	 * Get Default Category Archive Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_category_default_banner() {
		$category_banner_image_link = get_option( 'reendex_category_banner_image' );
		return empty( $category_banner_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $category_banner_image_link;
	}
}

if ( ! function_exists( 'reendex_get_search_default_banner' ) ) {
	/**
	 * Get Default Archive and Search Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_search_default_banner() {
		$banner_image_link = get_option( 'reendex_search_banner_image' );
		return empty( $banner_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $banner_image_link;
	}
}

if ( ! function_exists( 'reendex_get_default_single_banner' ) ) {
	/**
	 * Get Default Single Page Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_default_single_banner() {
		$banner_single_image_link = get_option( 'reendex_single_banner_image' );
		return empty( $banner_single_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $banner_single_image_link;
	}
}

if ( ! function_exists( 'reendex_get_default_blog_banner' ) ) {
	/**
	 * Get Default Blog Page Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_default_blog_banner() {
		$banner_blog_image_link = get_option( 'reendex_blog_banner_image' );
		return empty( $banner_blog_image_link ) ? get_template_directory_uri() . '/img/banner.jpg' : $banner_blog_image_link;
	}
}

if ( ! function_exists( 'reendex_get_default_contact_banner' ) ) {
	/**
	 * Get Default Contact Page Banner
	 *
	 * @return mixed|string|void
	 */
	function reendex_get_default_contact_banner() {
		$banner_contact_image_link = get_option( 'reendex_contact_banner_image' );
		return empty( $banner_contact_image_link ) ? get_template_directory_uri() . '/img/banner_contact.jpg' : $banner_contact_image_link;
	}
}

if ( ! function_exists( 'reendex_content_start' ) ) :
	/**
	 * Site content codes
	 */
	function reendex_content_start() {
		?>
		<div id="content" class="site-content">
		<?php
	}
endif;
add_action( 'reendex_content_start_action', 'reendex_content_start', 10 );

if ( ! function_exists( 'reendex_content_end' ) ) :
	/**
	 * Site content codes
	 */
	function reendex_content_end() {
		?>
		</div><!-- #content -->
		<?php
	}
endif;
add_action( 'reendex_content_end_action', 'reendex_content_end', 10 );

if ( ! function_exists( 'reendex_footer_start' ) ) :
	/**
	 * End div id #content
	 */
	function reendex_footer_start() {
	?>
		<footer id="footer" class="site-footer">
	<?php
	}
endif;
add_action( 'reendex_footer_start', 'reendex_footer_start', 10 );

if ( ! function_exists( 'reendex_footer_bottom_start' ) ) :
	/**
	 * Start div id #parallax-section2
	 */
	function reendex_footer_bottom_start() {
		$footer_background_image = get_theme_mod( 'reendex_footer_background_image' );
		$attachment_url = attachment_url_to_postid( $footer_background_image );
		$image_alt = get_post_meta( $attachment_url, '_wp_attachment_image_alt', true );
		?>
	   <div id="parallax-section2">
			<div class="bg parallax2 overlay img-overlay2">
				<img class="background-image" src="<?php echo esc_url( $footer_background_image ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" /> 
				<div class="footer-widget-area">
					<div class="container">
					<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex-footer_widgets', 'disable' ) ) ) { ?>
						<div class="row">
						<?php reendex_footer_widgets(); ?>
						</div><!-- /.row -->
					<?php }?>
					</div><!-- /.container -->
				</div><!-- /.footer-widget-area -->
			</div><!-- /.bg parallax2  -->                      
		</div><!-- /#parallax-section2 -->
	<?php }
endif;
add_action( 'reendex_footer', 'reendex_footer_bottom_start', 11 );

if ( ! function_exists( 'reendex_copyright' ) ) :
	/**
	 * Start div id #copyrights
	 */
	function reendex_copyright() {
	?>
	<div id="copyrights"> 
		<div class="container"> 
			<div class="row no-gutter">
				<div class="col-sm-12 col-md-8">
					<div class="copyright"> 
						<p class="copyrights">
							<?php
							$options = reendex_get_theme_options();
							$copyright = $options['reendex_copyright_text'];
							if ( '' !== $copyright && ! empty( $copyright ) ) {
								echo wp_kses_post( $copyright );
							} else { ?>
								<?php
								$site_link = '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name' ) ) . '" rel="nofollow">' . esc_attr( get_bloginfo( 'name' ) ) . '</a>';
								/* translators: 1. date, 2. blog name */
								printf( esc_html__( 'Copyright &copy; %1$s %2$s. All Rights Reserved.', 'reendex' ), esc_html( date( 'Y' ) ), wp_kses_post( $site_link ) );
								?>
								<?php
								/* translators: reendex is default value for 'Theme by' */
								printf( esc_html__( 'Theme by %1$s', 'reendex' ), '<a href="' . esc_url( 'https://themeforest.net/user/via-theme' ) . '">Via-Theme</a>' ); ?>
							<?php } ?>
						</p>
					</div><!-- /.copyright -->
				</div><!-- /.col-sm-12 col-md-6 -->
			<?php
				$options = reendex_get_theme_options();
				$show_social = get_theme_mod( 'reendex_copyright_socialmedia_show', 'enable' );
			if ( 'enable' === $show_social ) : ?>
			<div class="col-sm-12 col-md-4">
				<?php get_template_part( 'template-parts/content', 'socialmedia' ); ?>
			</div>
			<?php endif;?>
			</div><!-- /.row no-gutter -->
		</div><!-- /.container -->
	</div><!-- /#copyrights -->
	<?php
	}
endif;
add_action( 'reendex_footer', 'reendex_copyright', 20 );

if ( ! function_exists( 'reendex_footer_end' ) ) :
	/**
	 * End footer id #footer
	 */
	function reendex_footer_end() {
		?>
		</footer><!-- end .site-footer -->
		<?php
	}
endif;
add_action( 'reendex_footer_end', 'reendex_footer_end', 100 );

if ( ! function_exists( 'reendex_loader' ) ) :
	/**
	 * Start div class .pageloader
	 */
	function reendex_loader() {
		$options = reendex_get_theme_options();
		if ( get_theme_mod( 'reendex_activate_loader', 1 ) ) { ?>
	<div class="pageloader">
		<div class="spinner">
			<div class="rect1"></div>
			<div class="rect2"></div>
			<div class="rect3"></div>
			<div class="rect4"></div>
			<div class="rect5"></div>
		</div>
	</div><!-- /.pageloader -->
		<?php }
	}
endif;
add_action( 'reendex_before_header', 'reendex_loader', 10 );

/**
 * Main Menu
 */
/**
 * Mobile Menu
 */
function reendex_mobile_menu() {
	wp_nav_menu(array(
		'theme_location' => 'mainmenu',
		'depth' => 0,
		'container_id' => 'dropdown',
		'fallback_cb' => 'Reendex_Bootstrap_Navwalker::fallback',
		'walker' => new Reendex_Bootstrap_Navwalker(),
	));
}
/**
 * Main Menu bottom
 */
function reendex_main_menu_bottom() {
	if ( has_nav_menu( 'mainmenubottom' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'mainmenubottom',
			'depth'             => 0,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}

/**
 * World Menu
 */
function reendex_world_bottom_menu() {
	if ( has_nav_menu( 'worldmenu' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'worldmenu',
			'depth'             => 5,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}
/**
 * News Menu
 */
function reendex_news_bottom_menu() {
	if ( has_nav_menu( 'newsmenu' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'newsmenu',
			'depth'             => 5,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}
/**
 * Sport Menu
 */
function reendex_sport_bottom_menu() {
	if ( has_nav_menu( 'sportmenu' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'sportmenu',
			'depth'             => 5,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}
/**
 * Health Menu
 */
function reendex_health_bottom_menu() {
	if ( has_nav_menu( 'healthmenu' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'healthmenu',
			'depth'             => 5,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}
/**
 * Travel Menu
 */
function reendex_travel_bottom_menu() {
	if ( has_nav_menu( 'travelmenu' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'travelmenu',
			'depth'             => 5,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}
/**
 * Entertainment Menu
 */
function reendex_entertainment_bottom_menu() {
	if ( has_nav_menu( 'entertainmentmenu' ) ) :
		wp_nav_menu(array(
			'theme_location'    => 'entertainmentmenu',
			'depth'             => 5,
			'container_id'      => 'dropdown',
			'fallback_cb'       => 'Reendex_Bootstrap_Navwalker::fallback',
			'walker'            => new Reendex_Bootstrap_Navwalker(),
		));
	endif;
}
?>
