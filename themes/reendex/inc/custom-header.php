<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Reendex
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses reendex_header_style()
 */
function reendex_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'reendex_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '777',
		'width'                  => 1920,
		'height'                 => 264,
		'flex-height'            => true,
		'wp-head-callback'       => 'reendex_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'reendex_custom_header_setup' );

if ( ! function_exists( 'reendex_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see reendex_custom_header_setup().
	 */
	function reendex_header_style() {
		$header_text_color = get_header_textcolor();

		/*
		 * If no custom options for text are set, let's bail.
		 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: HEADER_TEXTCOLOR.
		 */
		if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
			return;
		}

		// If we get this far, we have custom styles. Let's do this.
				// Has the text been hidden?
		if ( ! display_header_text() ) :
			$css = '.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}';
			// If the user has set a custom color for the text use that.
			else :
				$css = '.site-branding-text .site-title a,
				.site-description {
					color: #' . esc_attr( $header_text_color ) . '}';
			endif;

			wp_add_inline_style( 'reendex-style', $css );
	}
endif;
add_action( 'wp_enqueue_scripts', 'reendex_header_style', 10 );
