<?php
/**
 * Load Custom Functions required on theme.
 *
 * @package Reendex
 */

/**
 * Sticky sidebar options.
 */
function reendex_enqueue_sticky_sidebar() {
	$reendex_sticky_sidebar = get_theme_mod( 'reendex_sticky_sidebar', 'enable' );
	if ( 'enable' === $reendex_sticky_sidebar ) {
		$sidebar_nav_js = '';
		$sidebar_nav_js .= "
			// Sticky sidebar on scroll.
			( function ( $ ) {
				'use strict';
				$( window ).on('load', function() {
					$('.home-main-content').theiaStickySidebar({
						additionalMarginTop: 30
					});

					$('.widget-area').theiaStickySidebar({
						additionalMarginTop: 30
					});
				} );
			}( jQuery ) );
		";
		wp_add_inline_script( 'reendex-main', $sidebar_nav_js );
	}
}
add_action( 'wp_enqueue_scripts', 'reendex_enqueue_sticky_sidebar' );

/**
 * Menu options.
 */
function reendex_enqueue_hover_menu() {
	$reendex_hover_menu = get_theme_mod( 'reendex_hover_menu', 'enable' );
	if ( 'enable' === $reendex_hover_menu ) {
		$hover_menu_js = '';
		$hover_menu_js .= "
			// Add hover effect to menus.
			( function ( $ ) {
				'use strict';
				$( document ).ready( function () {
					$('ul.nav li.dropdown').hover(function() {
						$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn();
					},
					function() {
						$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut();
					});
				} );
			}( jQuery ) );
		";
		wp_add_inline_script( 'reendex-main', $hover_menu_js );
	}
}
add_action( 'wp_enqueue_scripts', 'reendex_enqueue_hover_menu' );

/**
 * Instagram Scrapper.
 *
 * @param string $username Username.
 * @param string $slice Slice.
 */
function reendex_scrape_instagram( $username, $slice = 9 ) {
	$username = trim( strtolower( $username ) );
	$username = str_replace( '@', '', $username );
	$instagram = get_transient( 'insta-a10-' . sanitize_title_with_dashes( $username ) );
	if ( false === $instagram ) {
		$remote = wp_remote_get( 'http://instagram.com/' . trim( $username ) );
		if ( is_wp_error( $remote ) ) {
			return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'reendex' ) );
		}
		if ( 200 !== wp_remote_retrieve_response_code( $remote ) ) {
			return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'reendex' ) );
		}
		$shards      = explode( 'window._sharedData = ', $remote['body'] );
		$insta_json  = explode( ';</script>', $shards[1] );
		$insta_array = json_decode( $insta_json[0], true );
		if ( ! $insta_array ) {
			return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'reendex' ) );
		}
		if ( isset( $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] ) ) {
			$images = $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
		} elseif ( isset( $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'] ) ) {
			$images = $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'];
		} else {
			return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'reendex' ) );
		}
		if ( ! is_array( $images ) ) {
			return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'reendex' ) );
		}
		$instagram = array();
		foreach ( $images as $image ) {
			if ( true === $image['node']['is_video'] ) {
				$type = 'video';
			} else {
				$type = 'image';
			}
			$caption = esc_html__( 'Instagram Image', 'reendex' );
			if ( ! empty( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] ) ) {
				$caption = wp_kses( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'], array() );
			}
			$instagram[] = array(
				'description' => $caption,
				'link'        => trailingslashit( '//instagram.com/p/' . $image['node']['shortcode'] ),
				'time'        => $image['node']['taken_at_timestamp'],
				'comments'    => $image['node']['edge_media_to_comment']['count'],
				'likes'       => $image['node']['edge_liked_by']['count'],
				'thumbnail'   => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][0]['src'] ),
				'small'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][2]['src'] ),
				'large'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][4]['src'] ),
				'original'    => preg_replace( '/^https?\:/i', '', $image['node']['display_url'] ),
				'type'        => $type,
			);
		} // End foreach().
		// do not set an empty transient - should help catch private or empty accounts.
		if ( ! empty( $instagram ) ) {
			set_transient( 'insta-a10-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'reendex_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );
		}
	} // End if().
	if ( ! empty( $instagram ) ) {
		return array_slice( $instagram, 0, $slice );
	} else {
		return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'reendex' ) );
	}
}

/**
 * Footer Widget Areas.
 */
if ( ! function_exists( 'reendex_footer_widgets' ) ) {
	/**
	 * Add footer widgets.
	 *
	 * @since 1.0
	 */
	function reendex_footer_widgets() {
		$footer_1 = '';
		$footer_2 = '';
		$footer_3 = '';
		$footer_4 = '';
		$footer_5 = '';
		$footer_class = '';
		$footer_columns = 0;

		if ( is_active_sidebar( 'footer_1' ) ) {
			$footer_1 = 1;
			$footer_columns++;
		}
		if ( is_active_sidebar( 'footer_2' ) ) {
			$footer_2 = 1;
			$footer_columns++;
		}
		if ( is_active_sidebar( 'footer_3' ) ) {
			$footer_3 = 1;
			$footer_columns++;
		}
		if ( is_active_sidebar( 'footer_4' ) ) {
			$footer_4 = 1;
			$footer_columns++;
		}
		if ( is_active_sidebar( 'footer_5' ) ) {
			$footer_5 = 1;
			$footer_columns++;
		}
		if ( 5 === $footer_columns ) {
			$footer_class = 'reendex-col-1-5 reendex-widget-col-1 reendex-footer-5-cols ';
		}
		if ( 4 === $footer_columns ) {
			$footer_class = 'reendex-col-1-4 reendex-widget-col-1 reendex-footer-4-cols ';
		} 
		elseif ( 3 === $footer_columns ) {
			$footer_class = 'reendex-col-1-3 reendex-widget-col-1 reendex-footer-3-cols ';
		} 
		elseif ( 2 === $footer_columns ) {
			$footer_class = 'reendex-col-1-2 reendex-widget-col-2 reendex-footer-2-cols ';
		}
		elseif ( 1 === $footer_columns ) {
			$footer_class = 'reendex-col-1-1 reendex-widget-col-2 reendex-footer-1-cols ';
		} 
		else {
			$footer_class = 'reendex-col-1-1 reendex-home-wide ';
		}
		if ( $footer_1 || $footer_2 || $footer_3 || $footer_4 ) {
			echo '<div class="reendex-footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">' . "\n";
				echo '<div class="reendex-container reendex-container-inner reendex-footer-widgets reendex-row reendex-clearfix">' . "\n";
			if ( $footer_1 ) {
				echo '<div class="' . esc_attr( $footer_class ) . ' reendex-footer-area reendex-footer-1">' . "\n";
					dynamic_sidebar( 'footer_1' );
				echo '</div>' . "\n";
			}
			if ( $footer_2 ) {
				echo '<div class="' . esc_attr( $footer_class ) . ' reendex-footer-area reendex-footer-2">' . "\n";
					dynamic_sidebar( 'footer_2' );
				echo '</div>' . "\n";
			}
			if ( $footer_3 ) {
				echo '<div class="' . esc_attr( $footer_class ) . ' reendex-footer-area reendex-footer-3">' . "\n";
					dynamic_sidebar( 'footer_3' );
				echo '</div>' . "\n";
			}
			if ( $footer_4 ) {
				echo '<div class="' . esc_attr( $footer_class ) . ' reendex-footer-area reendex-footer-4">' . "\n";
					dynamic_sidebar( 'footer_4' );
				echo '</div>' . "\n";
			}
			if ( $footer_5 ) {
				echo '<div class="' . esc_attr( $footer_class ) . ' reendex-footer-area reendex-footer-5">' . "\n";
					dynamic_sidebar( 'footer_5' );
				echo '</div>' . "\n";
			}
				echo '</div>' . "\n";
			echo '</div>' . "\n";
		}
	}
} // End if().

/**
 * Random string.
 *
 * @param int $length Random string length.
 * @return string
 */
function reendex_get_random_string( $length = 6 ) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$characters_length = strlen( $characters );
	$random_string = '';
	for ( $i = 0; $i < $length; $i++ ) {
		$random_string .= $characters[ rand( 0, $characters_length - 1 ) ];
	}
	return $random_string;
}

/**
 * Show metabox if post format equals provided value
 *
 * @param bool  $display To display or not.
 * @param array $post_format Name of the post format.
 * @return bool Display metabox.
 */
function reendex_show_on_post_format( $display, $post_format ) {
	if ( ! isset( $post_format['show_on']['key'] ) ) {
		return $display;
	}
	$post_id = 0;
	$value  = get_post_format( $post_id );
	if ( empty( $post_format['show_on']['key'] ) ) {
		return (bool) $value;
	}
	return $value == $post_format['show_on']['value'];
}
add_filter( 'cmb2_show_on', 'reendex_show_on_post_format', 10, 2 );
