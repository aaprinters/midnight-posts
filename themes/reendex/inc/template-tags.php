<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function reendex_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}
		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);
			// Wrap the time string in a link, and preface it with 'Posted on'.
			$posted_on = sprintf(
				/* translators: %s : description of posted date */
				esc_html_x( 'Posted on %s', 'post date', 'reendex' ),
				'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
			);
		$byline = sprintf(
			/* translators: %s : description of author */
			esc_html_x( 'by %s', 'post author', 'reendex' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);
		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
	}
	endif;

if ( ! function_exists( 'reendex_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function reendex_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'reendex' ) );
			if ( $categories_list && reendex_categorized_blog() ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'reendex' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
		}
		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link( esc_html__( 'Leave a comment', 'reendex' ), esc_html__( '1 Comment', 'reendex' ), esc_html__( '% Comments', 'reendex' ) );
			echo '</span>';
		}
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'reendex' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'reendex_content_nav' ) ) :
	/**
	 * Display navigation to next/previous pages when applicable
	 */
	function reendex_content_nav() {
		global $wp_query, $post;

		// Don't print empty markup on single pages if there's nowhere to navigate.
		if ( is_single() ) {
			$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
			$next = get_adjacent_post( false, '', false );
			if ( ! $next && ! $previous ) {
				return;
			}
		}
		// Don't print empty markup in archives if there's only one page.
		if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) ) {
			return;
		}
		$nav_class = ( is_single() ) ? 'navigation-post navigation-paging' : 'navigation-paging';
	?>
	<div class="<?php echo esc_attr( $nav_class ); ?>">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Post navigation', 'reendex' ); ?></h2>
		<?php if ( is_single() ) : // navigation links for single posts. ?>
			<div class="row vdivide">
				<?php previous_post_link( '<div class="nav-previous col-sm-6 col-md-6 col-lg-6">%link</div>', '<div class="nav-wrapper">' . esc_html__( 'Previous post', 'reendex' ) . '<h3 class="post-title">%title</h3></div>' ); ?>
				<?php next_post_link( '<div class="nav-next col-sm-6 col-md-6 col-lg-6">%link</div>', '<div class="nav-wrapper">' . esc_html__( 'Next post', 'reendex' ) . '<h3 class="post-title">%title</h3></div>' ); ?>
			</div><!-- /.row vdivide -->
		<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages. ?>
			<div class="row">
				<div class="col-md-12">
					<div class="nav-previous">
						<?php if ( get_next_posts_link() ) : ?>
							<?php next_posts_link( esc_html__( 'Older posts', 'reendex' ) ); ?>
						<?php endif; ?>
					</div>
					<div class="nav-next">
						<?php if ( get_previous_posts_link() ) : ?>
							<?php previous_posts_link( esc_html__( 'Newer posts', 'reendex' ) ); ?>
						<?php endif; ?>
					</div>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		<?php endif; ?>
		<div class="clear"></div>
	</div>
	<?php
	}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function reendex_categorized_blog() {
	$all_the_cool_cats = get_transient( 'reendex_categories' );
	if ( false === $all_the_cool_cats ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );
		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );
		set_transient( 'reendex_categories', $all_the_cool_cats );
	}
	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so reendex_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so reendex_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in reendex_categorized_blog.
 */
function reendex_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'reendex_categories' );
}
add_action( 'edit_category', 'reendex_category_transient_flusher' );
add_action( 'save_post',     'reendex_category_transient_flusher' );
