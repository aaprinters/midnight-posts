<?php
/**
 * Tag Cloud Widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_tag_cloud_widgets() {
	register_widget( 'reendex_Tag_Cloud_Widget' );
}
	add_action( 'widgets_init', 'reendex_tag_cloud_widgets' );

	/**
	 * Core class used to implement the Tag Cloud widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_Tag_Cloud_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'reendex_tag_cloud',
			'description'   => esc_html__( 'MNP: Tag Cloud Widget', 'reendex' ),
		);
		$control_ops = array(
			'id_base' => 'tag_cloud-widget',
			);
		parent::__construct( 'tag_cloud-widget', esc_html( 'MNP: Tag Cloud' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Tag Cloud widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current Tag Cloud widget instance.
	 */
	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		$number = $instance['number'];
		$smallest = $instance['smallest'];
		$largest = $instance['largest'];
		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		$args = array(
		 'smallest' => $smallest,
		 'largest'  => $largest,
		 'unit'     => 'px',
		 'number'   => $number,
		);
		?>
			<div class="tagcloud <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
				<?php
				if ( $title ) {
						echo '<h4 class="widget-title">' . esc_html( $title ) . '</h4>';
				}
				?>
				<div class="tag-cloud-wrap">
					<?php wp_tag_cloud( $args ); ?>
				</div>
			</div><!-- /.tagcloud -->
		
		<?php
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
		wp_reset_postdata();
	}

	/**
	 * Handles updating the settings for the current Tag Cloud widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']      = sanitize_text_field( $new_instance['title'] );
		$instance['number']     = absint( $new_instance['number'] );
		$instance['smallest']   = absint( $new_instance['smallest'] );
		$instance['largest']    = absint( $new_instance['largest'] );
		$instance['extclass']   = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Tag Cloud widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title'     => esc_attr__( 'Tags', 'reendex' ),
			'number'    => 4,
			'smallest'  => 8,
			'largest'   => 14,
			'extclass'  => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" /> 
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of tags to show', 'reendex' ); ?> </label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['number'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'largest' ) ); ?>"><?php esc_html_e( 'Largest:','reendex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'largest' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'largest' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['largest'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'smallest' ) ); ?>"><?php esc_html_e( 'Smallest:','reendex' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'smallest' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'smallest' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['smallest'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
		</p>
	<?php
	}
}
?>
