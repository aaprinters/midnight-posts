<?php
/**
 * About Us widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_about_us_widgets() {
	register_widget( 'reendex_About_Us_Widget' );
}
	add_action( 'widgets_init', 'reendex_about_us_widgets' );

	/**
	 * Core class used to implement the About Us widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_About_Us_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'   => 'reendex-about-us-widget',
			'description' => esc_html__( 'MNP: About Us Widget','reendex'
			),
		);
		$control_ops = array(
			'id_base' => 'reendex-about-us-widget',
			);
		parent::__construct( 'reendex-about-us-widget', esc_html( 'MNP: About Us' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current About Us widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current About Us widget instance.
	 */
	function widget( $args, $instance ) {
		$title    = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		?>
			<li class="reendex-about-us-widget <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
				<?php
				if ( $title ) {
						echo '<h4 class="widget-title">' . esc_html( $title ) . '</h4>';
				}
				?>
				<p>
					<?php echo esc_attr( $instance['about_us_text'] ); ?>
				</p>
			</li><!-- /.reendex-about-us-widget -->
		
		<?php
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
	}

	/**
	 * Handles updating the settings for the current About Us widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']          = sanitize_text_field( $new_instance['title'] );
		$instance['about_us_text']  = sanitize_text_field( $new_instance['about_us_text'] );
		$instance['extclass']       = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the About Us widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title'         => esc_html__( 'About Us', 'reendex' ),
			'about_us_text' => '',
			'extclass' 	    => '',
		);
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" /> 
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'about_us_text' ) ); ?>">
				<?php esc_html_e( 'About Us Text:','reendex' ); ?>
			</label>
			<textarea class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'about_us_text' ) ); ?>" id="<?php echo esc_attr( $this->get_field_name( 'about_us_text' ) ); ?>" cols="35" rows="5"><?php if ( '' !== $instance['about_us_text'] ) {echo esc_attr( $instance['about_us_text'] );} ?></textarea>
		</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
				<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
			</p>		
	<?php
	}
}
?>
