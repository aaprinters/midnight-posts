<?php
/*
Plugin Name: Midnight Post Custom
Plugin URI: http://logicsbuffer.com
description: Single post subcategories show plugin by logics Buffer
Version: 1.0
Author: Hassan Iqbal
Author URI: http://logicsbuffer.com
License: GPL2
*/
?>
<?php
function my_login_logo() { ?>
    <style type="text/css">
    #login h1 a,.login h1 a {
		background-image: none,url(http://allroundview.com/wp-content/uploads/2019/08/allroundviewlogofinal.png);
		background-position: bottom;
		margin: 0;
		padding: 0;
		width: 320px;
		background-size: contain;
	}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function replace_howdy( $wp_admin_bar ) {
    $my_account=$wp_admin_bar->get_node('my-account');
    $newtitle = str_replace( 'Howdy,', 'Welcome,', $my_account->title );
    $wp_admin_bar->add_node( array(
        'id' => 'my-account',
        'title' => $newtitle,
    ) );
}
add_filter( 'admin_bar_menu', 'replace_howdy',25 );

function role_admin_body_class( $classes ) {
	$user = wp_get_current_user();
	$user_role = $user->roles;
	$current_role = $user_role[0];
	 $classes .= $current_role;
 
    return $classes;
}
add_filter( 'admin_body_class', 'role_admin_body_class' );

function multisite_body_classes($classes) {
        $id = get_the_ID();
		$categories = get_the_category($id);
		$parent_cat = $categories[0]->category_parent;
		if($parent_cat == 0){
			$cat_name = $categories[0]->slug;
		}else{
			$cat_name = get_cat_name( $parent_cat );
			$cat_name = str_replace(" ","-",strtolower($cat_name)); ;
		}
        $classes[] = $cat_name;
        return $classes;
}
add_filter( 'body_class', 'multisite_body_classes' );

add_filter('list_terms_exclusions', 'yoursite_list_terms_exclusions', 10, 2);
function yoursite_list_terms_exclusions( $exclusions, $args ) {
  global $pagenow;
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('editorialeditor')) {
    $term_children = get_term_children(555,'category');
	array_push($term_children,'555');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
	
  }
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('add-inseditor')) {
	$term_children = get_term_children(642,'category');
	array_push($term_children,'642');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }
    if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('add-insmoderator')) {
	$term_children = get_term_children(642,'category');
	array_push($term_children,'642');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('videoeditor')) {
    $term_children = get_term_children(559,'category');
	array_push($term_children,'559');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }  
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('photoeditor')) {
    $term_children = get_term_children(558,'category');
	array_push($term_children,'558');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('magazineeditor')) {
    $term_children = get_term_children(1098,'category');
	array_push($term_children,'1098');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }  
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('libraryeditor')) {
    $term_children = get_term_children(967,'category');
	array_push($term_children,'967');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }  
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('librarymoderator')) {
    $term_children = get_term_children(967,'category');
	array_push($term_children,'967');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('blogger')) {
    $term_children = get_term_children(1045,'category');
	array_push($term_children,'1045');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('interviewer')) {
    $term_children = get_term_children(553,'category');
	array_push($term_children,'553');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }  
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('featurewriter')) {
    $term_children = get_term_children(977,'category');
	array_push($term_children,'977');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('reporter')) {
    $term_children = get_term_children(552,'category');
	array_push($term_children,'552');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  } 
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('writer')) {
   $term_children_article = get_term_children(118,'category');
   $term_children_blog = get_term_children(1045,'category');
   $term_children_magazine = get_term_children(1098,'category');
   $term_children_editorial = get_term_children(555,'category');
   $term_children = array_merge($term_children_article,$term_children_blog,$term_children_magazine,$term_children_editorial);
	array_push($term_children,'118','1045','1098','555');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }   
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('editor')) {
   $term_children_news = get_term_children(1062,'category');
   $term_children_videos = get_term_children(559,'category');
   $term_children_pictures = get_term_children(558,'category');
   $term_children = array_merge($term_children_news,$term_children_videos,$term_children_pictures);
	array_push($term_children,'1062','559','558');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }  
  if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('newseditor')) {
   $term_children = get_term_children(1062,'category');
	array_push($term_children,'1062');
	$categories = implode(',',$term_children);
	$exclusions =  "{$exclusions} AND t.term_id IN ({$categories})";
  }  
  /* if (in_array($pagenow,array('post.php','post-new.php')) && current_user_can('moderator')) {
    $exclusions = " {$exclusions} AND t.slug IN ('news-updates','interviews')";
  } */
  return $exclusions;
}

function news_reporter_meta_box() {
	$user = wp_get_current_user();
	$user_role = $user->roles;
	$current_role = $user_role[0];
	
		$roles_news_updates = array('newseditor','administrator');
		if (in_array($current_role, $roles_news_updates)) {
		add_meta_box(
			'global-notice',
			__( 'News Reports', 'sitepoint' ),
			'news_reporter_meta_box_callback',
			'post'
		);
	}
}
function news_reporter_meta_box_callback( $post ) {

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'news_reporter_nonce', 'news_reporter_nonce' );

    $value = get_post_meta( $post->ID, '_news_reporter', true );

    echo '<input style="width:60%" id="news_reporter" name="news_reporter" value="' . esc_attr( $value ) . '">';
}
////add_action( 'add_meta_boxes', 'news_reporter_meta_box' );

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id
 */
function save_news_reporter_meta_box_data( $post_id ) {
    // Sanitize user input.
	
    // Check if our nonce is set.
    if ( ! isset( $_POST['news_reporter_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['news_reporter_nonce'], 'news_reporter_nonce' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'post' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    }
    else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */

    // Make sure that it is set.
    if ( ! isset( $_POST['news_reporter'] ) ) {
        return;
    }
    $my_data =  $_POST['news_reporter'];

    // Update the meta field in the database.
    update_post_meta( $post_id, '_news_reporter', $my_data );
}

add_action( 'save_post', 'save_news_reporter_meta_box_data' );

	function show_sub_categories() {
		ob_start();
		global $post;
		
		// grab categories of current post
		$categories = get_the_category($post->ID);
		$parent_cat = $categories[0]->category_parent;
		$cat_name = get_cat_name( $parent_cat );
		if($parent_cat == 0){
			$cat_name = $categories[0]->name;
		}else{
			$cat_name = get_cat_name( $parent_cat );
		}

		if ($cat_name == 'Articles') {
			if ( is_active_sidebar( 'articles_single_page' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'articles_single_page' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'innerpage_1' ); ?>
				</div><!-- #common-sidebar -->
			<?php endif;
		}
		if ($cat_name == 'Reports') {
			if ( is_active_sidebar( 'reports_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'reports_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Interviews') {
			if ( is_active_sidebar( 'interviews_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'interviews_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Blog') {
			if ( is_active_sidebar( 'blog_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'blog_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Editorial') {
			if ( is_active_sidebar( 'editorial_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'editorial_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Public Voice') {
			if ( is_active_sidebar( 'publicvoice_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'publicvoice_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'In Pictures') {
			if ( is_active_sidebar( 'inpictures_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'inpictures_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}		
		if ($cat_name == 'In Videos') {
			if ( is_active_sidebar( 'invideos_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'invideos_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Stories') {
			if ( is_active_sidebar( 'stories_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'stories_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}	
		if ($cat_name == 'Add-ins') {
			if ( is_active_sidebar( 'add_ins_single_page' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'add_ins_single_page' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'News Updates') {
			if ( is_active_sidebar( 'leftsidebarnews_updates_single' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'leftsidebarnews_updates_single' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Megazine') {
			if ( is_active_sidebar( 'leftsidebarmagazinesingle' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'leftsidebarmagazinesingle' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Library') {
			if ( is_active_sidebar( 'leftsidebarlibrarysingle' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'leftsidebarlibrarysingle' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}
		if ($cat_name == 'Features') {
			if ( is_active_sidebar( 'leftsidebarfeaturessingle' ) ) : 
				?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
					 dynamic_sidebar( 'leftsidebarfeaturessingle' ); 
				?></div><?
			endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_1' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
		}

		if(is_archive() || is_category()){

			// grab categories of current post
			$categories = get_queried_object();
			$parent_cat = $categories->parent;
			if($parent_cat == 0){
				$cat_name = $categories->name;
			}else{
				$cat_name = get_cat_name( $parent_cat );
			}

			//print_r($category_archive);
			/* $parent_cat = $category_archive->parent;
			$cat_name = get_cat_name( $parent_cat ); */
			//echo $cat_name;
			
			if ($cat_name == 'Articles') {
			if ( is_active_sidebar( 'innerpage_articles' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_articles' ); ?>
					</div><!-- #common-sidebar -->
				<?php endif;
			if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'innerpage_1' ); ?>
				</div><!-- #common-sidebar -->
				<?php endif;
			}
			if ($cat_name == 'Reports') {
				if ( is_active_sidebar( 'innerpage_reports' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar( 'innerpage_reports' ); ?>							
						</div>
					<?php endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Interviews') {
				if ( is_active_sidebar( 'interviews_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'interviews_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Blog') {
				if ( is_active_sidebar( 'innerpage_blog' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'innerpage_blog' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Editorial') {
				if ( is_active_sidebar( 'editorial_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'editorial_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Public Voice') {
				if ( is_active_sidebar( 'publicvoice_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'publicvoice_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'In Pictures') {
				if ( is_active_sidebar( 'inpictures_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'inpictures_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}		
			if ($cat_name == 'In Videos') {
				if ( is_active_sidebar( 'invideos_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'invideos_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Stories') {
				if ( is_active_sidebar( 'stories_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'stories_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}	
			if ($cat_name == 'Add-ins') {
				if ( is_active_sidebar( 'add_ins_single_page' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'add_ins_single_page' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'News Updates') {
				if ( is_active_sidebar( 'leftsidebarnews_updates' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'leftsidebarnews_updates' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Magazine') {
				
				if ( is_active_sidebar( 'leftsidebarmagazinesingle' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'leftsidebarmagazinesingle' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Library') {
				if ( is_active_sidebar( 'leftsidebarlibrarysingle' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'leftsidebarlibrarysingle' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
			if ($cat_name == 'Features') {
				if ( is_active_sidebar( 'leftsidebarfeaturessingle' ) ) : 
					?><div id="left-sidebar" class="primary-sidebar widget-area" role="complementary"><?
						 dynamic_sidebar( 'leftsidebarfeaturessingle' ); 
					?></div><?
				endif;
				if ( is_active_sidebar( 'innerpage_1' ) ) : ?>
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'innerpage_1' ); ?>
						</div><!-- #common-sidebar -->
					<?php endif;
			}
		
		}
		
		return ob_get_clean();
	}
add_shortcode('show_subcat', 'show_sub_categories');


function check_user_role($roles, $user_id = null) {
	if ($user_id) $user = get_userdata($user_id);
	else $user = wp_get_current_user();
	if (empty($user)) return false;
	foreach ($user->roles as $role) {
		if (in_array($role, $roles)) {
			return true;
		}
	}
	return false;
}
add_action('admin_menu', 'wpdocs_register_my_custom_submenu_page');
 
function wpdocs_register_my_custom_submenu_page() {
	$user = wp_get_current_user();
	$user_role = $user->roles;
	$current_role = $user_role[0];
	
		$roles_news_updates = array('newsmoderator','newseditor','administrator');
		if (in_array($current_role, $roles_news_updates)) {
			add_menu_page('News Updates','News Updates', 'edit_posts','edit.php?category_name=news-updates','','dashicons-format-aside',5);
		}			
		
		$roles_news_updates = array('writer','contentmoderator','administrator');
		if (in_array($current_role, $roles_news_updates)) {
			add_menu_page('Articles','Articles','edit_posts','edit.php?category_name=articles');
			add_menu_page('Blog','Blog','edit_posts','edit.php?category_name=blog');
			add_menu_page('Magazine','Magazine','edit_posts','edit.php?category_name=magazine');
			add_menu_page('Editorial','Editorial','edit_posts','edit.php?category_name=editorial');
		}
		$roles_news_updates = array('editor','administrator');
		if (in_array($current_role, $roles_news_updates)) {
			add_menu_page('News Updates','News Updates', 'edit_posts','edit.php?category_name=news-updates','','dashicons-format-aside',5);
			add_menu_page('In Pictures','In Pictures','edit_posts','edit.php?category_name=in-pictures');
			add_menu_page('In Videos','In Videos','edit_posts','edit.php?category_name=in-videos');
		}
		
		$roles_reporter = array('reporter','administrator');
		if (in_array($current_role, $roles_reporter)) {
			add_menu_page('Reports','Reports','edit_posts','edit.php?category_name=reports');
		}
		
		$roles_featurewriter = array('featurewriter','administrator');
		if (in_array($current_role, $roles_featurewriter)) {		
			add_menu_page('Features','Features','edit_posts','edit.php?category_name=features');	
		}
		
		$roles_interviewer = array('interviewsmoderator','interviewer','administrator');
		if (in_array($current_role, $roles_interviewer)) {
			add_menu_page('Interviews','Interviews','edit_posts','edit.php?category_name=interviews');
		}
		
		$roles_blogger = array('blogger','administrator');
		if (in_array($current_role, $roles_blogger)) {
			add_menu_page('Blog','Blog','edit_posts','edit.php?category_name=blog');
		}
		
		$roles_libraryeditor = array('librarymoderator','libraryeditor','administrator');
		if (in_array($current_role, $roles_libraryeditor)) {
			add_menu_page('Library','Library','edit_posts','edit.php?category_name=library');
		}
		
		$roles_magazineeditor = array('magazineeditor','administrator');
		if (in_array($current_role, $roles_magazineeditor)) {
			add_menu_page('Magazine','Magazine','edit_posts','edit.php?category_name=magazine');
		}
		
		$roles_editorialeditor = array('editorialeditor','administrator');
		if (in_array($current_role, $roles_editorialeditor)) {
			add_menu_page('Editorial','Editorial','edit_posts','edit.php?category_name=editorial','','dashicons-format-aside',5);
		}
		
		$roles_photoeditor = array('mediamoderators','photoeditor','administrator');
		if (in_array($current_role, $roles_photoeditor)) {		
			add_menu_page('In Pictures','In Pictures','edit_posts','edit.php?category_name=in-pictures');
		}
		
		$roles_videoeditor = array('mediamoderators','videoeditor','administrator');
		if (in_array($current_role, $roles_videoeditor)) {
			add_menu_page('In Videos','In Videos','edit_posts','edit.php?category_name=in-videos');
		}
		
		$roles_add_ins = array('add-insmoderator','add-inseditor','administrator');
		if (in_array($current_role, $roles_add_ins)) {
			add_menu_page('Add-ins','Add ins','edit_posts','edit.php?category_name=add-ins');
		}
			
}
function posts_for_current_author($query) {
	global $pagenow;
	$user = wp_get_current_user();
	$user_role = $user->roles;
	$current_role = $user_role[0];

	$role_contentmoderator = array('contentmoderator');
	$role_newsmoderator = array('newsmoderator');
	$role_interviewsmoderator = array('interviewsmoderator');
	$role_librarymoderator = array('librarymoderator');
	$role_add_ins = array('add-insmoderator','add-inseditor');
	//$role_contentmoderator = array('contentmoderator');
	if (in_array($current_role, $role_contentmoderator)) {
		$taxquery = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array( 'articles','reports','features','articles','blogs','magazine','editorial')
			)
		);
		$query->set( 'tax_query', $taxquery );
		return $query;
	}elseif(in_array($current_role, $role_newsmoderator)) {

		$taxquery = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array( 'news-updates')
			)
		);
		$query->set( 'tax_query', $taxquery );
		return $query;
	}elseif(in_array($current_role, $role_interviewsmoderator)) {

		$taxquery = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array( 'interviews')
			)
		);
		$query->set( 'tax_query', $taxquery );
		return $query;
	}elseif(in_array($current_role, $role_add_ins)) {

		$taxquery = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array( 'add-ins')
			)
		);
		$query->set( 'tax_query', $taxquery );
		return $query;
	}elseif(in_array($current_role, $role_librarymoderator)){
	?><pre><?php print_r($current_role); ?></pre><?php

		$taxquery = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array( 'library')
			)
		);
		$query->set( 'tax_query', $taxquery );
		return $query;
	}elseif( 'edit.php' != $pagenow || !$query->is_admin ){
        return $query;
    }elseif( !current_user_can( 'manage_options' ) ) {
       global $user_ID;
       $query->set('author', $user_ID );
	}
     return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

?>